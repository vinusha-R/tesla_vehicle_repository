package tesla.vehicle.operations;

import java.io.*;
import java.util.*;

import tesla.main.Tesla;
import tesla.vehicle.Vehicle;
import tesla.vehicle.implementaion.*;

public abstract class VehicleOperations implements Vehicle {
	public String modelNo;
	public int vehicleNo, y;
	public int cost;
	public String comma = ",";
	public String new_line = "\n";
	public String file_attributes = "modelNo,vehicleNo,cost";

	// ListIterator iterator=list_arr.listIterator();

	public abstract void add();

	public void delete() {
		System.out.println("enter vehicle number you want to delete");

		Scanner scanner = new Scanner(System.in);
		int input = scanner.nextInt();
		ListIterator iterator = Tesla.list_arr.listIterator();
		while (iterator.hasNext()) {
			int i = 0;
			// System.out.println("hello");

			/*
			 * Car c = (Car)iter.next(); // ArrayList al = (ArrayList)iter.next(); //
			 * VehicleAbstract s = (VehicleAbstract)iter.next(); if(a == c.getVehicleNo()) {
			 * list_arr.remove(c); System.out.println("vehicle has been removed"); break;
			 */
			// Object c = iter.next();
			VehicleOperations vehicle = (VehicleOperations) iterator.next();
			if (vehicle instanceof Car) {
				// System.out.println("inside if condition");
				Car car = (Car) vehicle;
				// System.out.println(c1.getVehicleNo());
				if (input == car.getVehicleNo()) {
					// System.out.println(a);
					// System.out.println(c1.getVehicleNo());
					iterator.remove();
					System.out.println("vehicle has been removed");
					y = 1;
					break;
				}
			} else if (vehicle instanceof Truck) {
				Truck truck = (Truck) vehicle;
				if (input == truck.getVehicleNo()) {
					iterator.remove();
					System.out.println("vehicle has been removed");
					y = 1;
					break;
				}
			} else if (vehicle instanceof Bus) {
				Bus b = (Bus) vehicle;
				if (input == b.getVehicleNo()) {
					iterator.remove();
					System.out.println("vehicle has been removed");
					y = 1;
					break;
				}
			}
		}
		if (y == 0) {
			System.out.println("vehicle not available");
		}
	}

	public void update() {
		int y = 0;
		System.out.println("enter the vehicle number to update");
		Scanner scanner = new Scanner(System.in);
		int input = scanner.nextInt();
		ListIterator iterator = Tesla.list_arr.listIterator();

		while (iterator.hasNext()) {

			/*
			 * VehicleAbstract s = (VehicleAbstract)iter.next(); if(a == s.getVehicleNo()) {
			 * System.out.println("enter new vehicle number"); Scanner sc1 = new
			 * Scanner(System.in); int a1 = sc1.nextInt(); s.setVehicleNo(a1); break;
			 */
			VehicleOperations vehicle = (VehicleOperations) iterator.next();
			if (vehicle instanceof Car) {
				// System.out.println("inside if");
				Car car = (Car) vehicle;
				if (input == car.getVehicleNo()) {
					System.out.println("enter new vehicle number");
					int vehicleNum = scanner.nextInt();
					car.setVehicleNo(vehicleNum);
					System.out.println("vehicle has been updated");
					y = 1;
					break;
				}
			} else if (vehicle instanceof Truck) {
				Truck truck = (Truck) vehicle;
				if (input == truck.getVehicleNo()) {
					System.out.println("enter new vehicle number");
					int vehicleNum = scanner.nextInt();
					truck.setVehicleNo(vehicleNum);
					System.out.println("vehicle has been updated");
					y = 1;
					break;
				}
			} else if (vehicle instanceof Bus) {
				Bus bus = (Bus) vehicle;
				if (input == bus.getVehicleNo()) {
					System.out.println("enter new vehicle number");
					int vehicleNum = scanner.nextInt();
					bus.setVehicleNo(vehicleNum);
					System.out.println("vehicle has been updated");
					y = 1;
					break;
				}
			}
		}
		if (y == 0) {
			System.out.println("vehicle not available");
		}

	}

	public void find() {
		int y = 0;
		System.out.println("enter the vehicle number to find");
		Scanner scanner = new Scanner(System.in);
		int vehicleNum = scanner.nextInt();
		ListIterator iterator = Tesla.list_arr.listIterator();
		/*
		 * for( int i=0;i<list_arr.size();i++) { VehicleAbstract s =
		 * (VehicleAbstract)list_arr.get(i); if(a == s.getVehicleNo()) {
		 * System.out.println("vehicle found"); break; } //s.getVehicleNo();
		 * //System.out.println(list_arr.get(i).getVehicleNo);
		 * System.out.println("vehicle not found"); }
		 */
		while (iterator.hasNext()) {
			VehicleOperations vehicle = (VehicleOperations) iterator.next();
			/*
			 * VehicleAbstract s = (VehicleAbstract)iter.next(); if(a == s.getVehicleNo()) {
			 * System.out.println("vehicle found"); break; }
			 */
			if (vehicle instanceof Car) {
				Car car = (Car) vehicle;
				if (vehicleNum == car.getVehicleNo()) {
					System.out.println("vehicle found");
					System.out.println(car);
					y = 1;
					break;
				}
			} else if (vehicle instanceof Truck) {
				Truck truck = (Truck) vehicle;
				if (vehicleNum == truck.getVehicleNo()) {
					System.out.println("vehicle found");
					System.out.println(truck);
					y = 1;
					break;
				}
			} else if (vehicle instanceof Bus) {
				Bus bus = (Bus) vehicle;
				if (vehicleNum == bus.getVehicleNo()) {
					System.out.println("vehicle found");
					System.out.println(bus);
					y = 1;
					break;
				}
			}

		}
		if (y == 0) {
			System.out.println("vehicle not available");
		}
	}

	public void show() {
		// while (iter.hasNext()) {
		// System.out.println("hello");
		// System.out.println(iter.next());

		/*
		 * if(iter.next() instanceof Car) { Car s = (Car)iter.next();
		 * System.out.println(s.getVehicleNo()); }
		 */
		// VehicleAbstract s = (VehicleAbstract)list_arr.get(0);
		// System.out.println(s.getVehicleNo());

		for (int i = 0; i < Tesla.list_arr.size(); i++) {
			System.out.println(Tesla.list_arr.get(i));
		}

		// ArrayList al = (ArrayList)iter.next();
		// System.out.println(al.get(0));
		// System.out.println(list_arr.get(0));
		// System.out.println(list_arr.get(0).vehicleNo);
		// }

	}

	public void writeFile(String fileName) {

		ListIterator iterator = Tesla.list_arr.listIterator();

		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(fileName);
			fileWriter.append(file_attributes);
			// fw.append(new_line);
			fileWriter.append(System.lineSeparator());
			while (iterator.hasNext()) {
				// ListIterator li = (ListIterator)iterator.next();
				VehicleOperations vehicle = (VehicleOperations) iterator.next();
				if (vehicle instanceof Car) {
					Car car = (Car) vehicle;
					fileWriter.append(car.getModelNo());
					fileWriter.append(comma);
					fileWriter.append(String.valueOf(car.vehicleNo));
					fileWriter.append(comma);
					fileWriter.append(String.valueOf(car.cost));
					// fw.append(new_line);
					fileWriter.append(System.lineSeparator());
				} else if (vehicle instanceof Truck) {
					Truck truck = (Truck) vehicle;
					fileWriter.append(truck.getModelNo());
					fileWriter.append(comma);
					fileWriter.append(String.valueOf(truck.vehicleNo));
					fileWriter.append(comma);
					fileWriter.append(String.valueOf(truck.cost));
					// fw.append(new_line);
					fileWriter.append(System.lineSeparator());
				} else if (vehicle instanceof Bus) {
					Bus bus = (Bus) vehicle;
					fileWriter.append(bus.getModelNo());
					fileWriter.append(comma);
					fileWriter.append(String.valueOf(bus.vehicleNo));
					fileWriter.append(comma);
					fileWriter.append(String.valueOf(bus.cost));
					// fw.append(new_line);
					fileWriter.append(System.lineSeparator());
				}

			}
			/*
			 * for(Player p :(Player)al) {
			 * 
			 * }
			 */
		} catch (IOException e) {

		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {

			}
		}
		System.out.println("file has been written successfully");
	}

}
